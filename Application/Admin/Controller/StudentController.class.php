<?php
namespace Admin\Controller;
use Admin\Model\StudentModel;
use Think\Controller;

class StudentController extends Controller{
    /**
     * 显示添加学生页面模板
     */
    public function index(){
        $this->display();
    }

    /**
     *创建学生的方法
     */
    public function addStudent(){
        $Student = new StudentModel();
        $Student->createStudent();
        $username = I('post.username');
        $mobile=I('post.mobile');
        echo '姓名：'.$username;
        echo '电话：'.$mobile;
    }

    /**
     * 显示修改学生信息页面模板
     */
    public function update(){
        $this->display();
    }
    public function fetchList(){
       
    }
}