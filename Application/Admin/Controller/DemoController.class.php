<?php
namespace Admin\Controller;
use Think\Controller;
class DemoController extends Controller{

//添加操作
public function add(){
//实例化manager模型，manager为数据库中的manager表
$managerModel=M("manager");
//create() ：根据表单提交的POST数据创建数据对象
$data = $managerModel->create();
//add() ：写入数据到news数据库中
if($managerModel->add($data)){
/**
* success() 和error()的有三个参数
* 第一个参数表示提示信息
* 第二个参数表示跳转地址
* 第三个参数是跳转时间（单位为秒）
* */
$this->success('添加成功','all',5);
}else{
$this->error('添加失败');
}
}
//读取操作（read）
public function all(){
$newsModel=M('news');
//$newsModel->select(): select * from news
$data=$newsModel->select();
//assign(): 分配数据(相当于赋值操作)
$this->assign('news',$data);
$this->display();
}

/**
* 更新操作
* 注意：在对应的静态页中对应的form表单中要添加一句
* <input type=”hidden”name=”id”value=”{$news.id}”>
* 其中name的值为news表的主键，value的值为控制器中传过来的值
* 是数据库中对应的主键。
**/
public function edit(){
if(IS_POST){
$newsModel=M("news");
$data=$newsModel->create();
//save() ：根据条件更新记录
if($newsModel->save($data)){
$this->success('修改成功','all',3);
}else{
$this->error('修改失败');
}
}else{
/**
* I()：Thinkphp中简单的获取参数的方式
* I(“post.id”): $_POST[‘id’];
* I(“get.id”): $_GET[‘id’];
* param变量类型是框架特有的支持自动判断当前请求类型的变量获取方式
**/
$id = I('id'); //等同于I( 'param.id')
$newsModel = M("news");
//find($id):读取id = $id 的新闻数据
$data = $newsModel->find($id);
$this->assign('news',$data);
$this->display();
}
}
//删除操作（delete）
public function del(){
$newsModel = M('news');
$id = I('id');
if(isset($id) && $newsModel->delete($id)){
$this->success('删除成功');
}else{
$this->error('删除失败');
}
}
}