<?php
namespace Admin\Controller;
use Admin\Model\ManagerModel;
use Think\Controller;

class ManagerController extends Controller{
    /**
     * 显示添加管理员页面模板
     */
    public function index(){
        $this->display();
    }

    /**
     *创建学生的方法
     */
    public function addManager(){
        $Manager = new ManagerModel();
        $Manager->createManager();
        $username = I('post.username');
        $mobile=I('post.mobile');
        echo '姓名：'.$username;
        echo '电话：'.$mobile;
    }
}