<?php
namespace Admin\Model;
use Think\Model;
class ManagerModel extends Model{
    /**
     * success()方法
     */
    function success(){
        $result=[
            'error' => 0,
            'message' => '成功'
        ];
        return json_encode($result);
    }
    /**
     * error()方法
     */
    function error($mag){
        $data = [
            'error' => 1,
            'message' => $mag
        ];
        return json_encode($data);
    }
    /**
     *添加管理员信息的方法
     */
    public function createManager(){
        $username=I('post.username');
        $password=I('post.password');
        $confirm=I('post.confirm');
        $mobile=I('post.mobile');
        $manager=M('Manager');

        {/* 将数据写入数组*/ }
        $data['username']=$username;
        $data['password']=$password;
        $data['mobile']=$mobile;

        {/* 判断用户不能为空*/ }
        if(empty($username)){
            die($this->error('用户不能为空！'));
        }
        {/* 判断密码不能为空*/ }
        if(empty($password)){
            die($this->error('密码不能为空！'));
        }
        {/* 判断确认密码不能为空*/ }
        if(empty($confirm)){
            die($this->error('确认密码不能为空！'));
        }
        {/* 判断密码是否一致*/ }
        if($password!=$confirm){
            die($this->error('密码不一致！'));
        }
        {/* 判断电话不能为空*/ }
        if(empty($mobile)){
            die($this->error('电话不能为空！'));
        }
        {/* 判断账号是否存在*/ }
        if($manager->where(array("mobile"=>$mobile))->select()){
            die($this->error('账号已存在！'));
        }
        else{
            $manager->data($data)->add();

        }
        echo $this->success();
    }

}
